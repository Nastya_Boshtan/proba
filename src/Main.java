import java.math.BigInteger;
import java.util.ArrayList;

public class Main {
    static char[] alphabet = {'А','Б','В','Г','Д','Е','Ї','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Є','Ґ','Ь','І','Ю','Я',' ','0','1','2','3','4','5','6','7','8','9'};

    public static ArrayList<Integer> Encrypted (String message){
        ArrayList<Integer> end = new ArrayList<>();
        for(int i=0; i<message.length(); i++) {
            for (int j = 0; j < alphabet.length; j++) {
                if (alphabet[j] == message.charAt(i)) {

                    end.add(j+1);
                }
            }
        }
        return end;
    }
    public static Integer Ezp(ArrayList <Integer> mesage, int[] public_key, int[] private_key){
        ArrayList <Integer> newIndex = new ArrayList<>();
        int h=0;
        int h1=0;
        for(int i=0;i<mesage.size();i++){
            h=(int)Math.pow(h1+mesage.get(i),2);
            h1=Math.floorMod(h,public_key[1]);
            newIndex.add(h1);
        }
        System.out.println("Обчислення хеш-образу");
        System.out.println(newIndex);
        h=newIndex.get(mesage.size()-1);
        System.out.println("Хеш-образ");
        System.out.println(h);
        BigInteger sPrimary = null;
        BigInteger s = null;
        sPrimary=new BigInteger(String.valueOf(h));
        s=(sPrimary.pow(private_key[0]).mod(BigInteger.valueOf(private_key[1])));
        System.out.println("Цифровий підпис");
        System.out.println(s);
        return (h);
    }

    public static void main(String[] args) {
     //створення ключів
     int p=29;
     int q = 73;
     int e=23;
     int n = p*q;
     int function = (p-1)*(q-1);
     int[] public_key = new int[2];
     public_key[0]=e;
     public_key[1]=n;
     System.out.println("Відкритий ключ шифру");
     for(int i=0;i<2;i++){
        System.out.print(public_key[i] + " ");
     }
        System.out.println();
     int k=1;
     double d=((((double)k*(double)function)+1)/(double)e);
     while(d!=(int)d){
         k++;
         d=((((double)k*(double)function)+1)/(double)e);
     }
        int[] private_key = new int[2];
        private_key[0]=(int) d;
        private_key[1]=n;
        System.out.println("Закритий ключ шифру");
        for(int i=0;i<2;i++){
            System.out.print(private_key[i] + " ");
        }
        System.out.println();
      //шифрація
      String message = "ДКБ18П";
      ArrayList<Integer> oldPosition = new ArrayList<>();
      oldPosition=Encrypted(message);
        ArrayList <BigInteger> newPosition = new ArrayList<>();
        BigInteger c = null;
        for(int i=0;i<oldPosition.size();i++) {
           c=new BigInteger(String.valueOf(oldPosition.get(i)));
            newPosition.add(c.pow(public_key[0]));

        }
        ArrayList<BigInteger> position = new ArrayList<>();
        for(int i=0;i<newPosition.size();i++){
          position.add(newPosition.get(i).mod(BigInteger.valueOf(public_key[1])));
        }
        System.out.println("Зашифроване повідомлення");
        System.out.println(position);

        //дешифрування
        ArrayList<Integer> decrypted = new ArrayList<>();
        decrypted.add(352);
        decrypted.add(254);
        decrypted.add(1621);
        decrypted.add(1868);
        decrypted.add(533);
        decrypted.add(1772);
        decrypted.add(1608);
        ArrayList <BigInteger> decryptedPosition = new ArrayList<>();
        BigInteger m = null;
        for(int i=0;i<decrypted.size();i++) {
            m=new BigInteger(String.valueOf(decrypted.get(i)));
            decryptedPosition.add(m.pow(private_key[0]));
        }
        ArrayList<BigInteger> decposition = new ArrayList<>();
        for(int i=0;i<decryptedPosition.size();i++){
            decposition.add(decryptedPosition.get(i).mod(BigInteger.valueOf(private_key[1])));
        }
        ArrayList<Integer> findsymbol= new ArrayList<>();
        for(int i =0;i<decposition.size();i++){
            findsymbol.add(decposition.get(i).intValue());
        }
        String end="";
        for(int i=0;i<findsymbol.size();i++){
            end+=alphabet[findsymbol.get(i)-1];
        }
        System.out.println("Розшифроване повідомлення");
        System.out.println(end);
        //хеш-образ повідомлення
        String dodatkove= "Р27";
        ArrayList <Integer> index = new ArrayList<>();
        index=Encrypted(dodatkove);
        System.out.println();
        System.out.println("Додаткове завдання 1.1");
        int ezp1=Ezp(index,public_key,private_key);
        //перевірка автентичності
        System.out.println();
        System.out.println("Додаткове завдання 1.2");
        int ezp=1222;
        String stringOfSymbol="ДВ30";
        BigInteger hPrimary = null;
        BigInteger hEzp = null;
        hPrimary=new BigInteger(String.valueOf(ezp));
        hEzp=(hPrimary.pow(public_key[0]).mod(BigInteger.valueOf(private_key[1])));
        System.out.println("Хеш-образ з ЕЦП");
        System.out.println(hEzp);
        ArrayList <Integer> ind = new ArrayList<>();
        ind=Encrypted(stringOfSymbol);

        int ezp2=Ezp(ind,public_key,private_key);
        if(hEzp.intValue()==ezp2){
            System.out.println("ПІд час передачі повідомлення не було спотворене");
        }else{
            System.out.println("ПІд час передачі повідомлення було спотворене випадково чи спеціально");
        }
    }
}
